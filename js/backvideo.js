var video = document.getElementById("myVideo");
var btn = document.getElementById("myBtn");
  video.pause();

function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = '<i class="icofont-ui-play"></i>';
  } else {
    video.pause();
    btn.innerHTML = '<i class="icofont-ui-pause"></i>';
  }
}